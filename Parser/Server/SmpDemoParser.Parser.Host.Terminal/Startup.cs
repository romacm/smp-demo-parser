﻿using System.IO;
using System.Threading;
using SmpDemoParser.Parser.Core.Implements;

namespace SmpDemoParser.Parser.Host.Terminal
{
    public class Startup
    {
        private const string DemoFilePath = @"../../../../DemoSamples/Demo.dem";
        public static void Main(string[] args)
        {
            using (var ms = new MemoryStream())
            using (var fileStream = new FileStream(DemoFilePath, FileMode.Open))
            {
                fileStream.CopyTo(ms);
                var parser = new DemoParser();
                parser.Parse(ms, CancellationToken.None).GetAwaiter().GetResult();
            }
        }
    }
}
