﻿using System;

namespace SmpDemoParser.Parser.Core.Models
{
    [Serializable]
    public class HeaderValidationException : Exception
    {
        public HeaderValidationException()
        {
        }

        public HeaderValidationException(string message) : base(message)
        {
        }

        public HeaderValidationException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}
