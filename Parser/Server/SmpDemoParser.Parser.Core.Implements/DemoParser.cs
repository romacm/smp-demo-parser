﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using SmpDemoParser.Parser.Core.Models;
using SmpDemoParser.Parser.Core.Sdk;

namespace SmpDemoParser.Parser.Core.Implements
{
    public class DemoParser : IDemoParser
    {
        public Task Parse(MemoryStream demo, CancellationToken ct)
        {
            var handle = GCHandle.Alloc(demo.GetBuffer(), GCHandleType.Pinned);
            var handleAddr = handle.AddrOfPinnedObject();
            try
            {
                // Parse header
                var header = Marshal.PtrToStructure<DemoHeader>(handleAddr);
                header.Validate();

                Console.WriteLine(header.DemoFileStamp);
            }
            finally
            {
                handle.Free();
            }
            throw new System.NotImplementedException();
        }
    }
}
