﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace SmpDemoParser.Parser.Core.Sdk
{
    public interface IDemoParser
    {
        Task Parse(MemoryStream demo, CancellationToken ct);
    }
}
