﻿using System;
using System.Runtime.InteropServices;

namespace SmpDemoParser.Parser.Core.Models
{
    [StructLayout(LayoutKind.Sequential, Pack = 4, Size = 1072)]
    public class DemoHeader
    {
        /// <summary>
        /// Header
        /// </summary>
        /// <remarks>
        /// 8 characters, should be DemoConstants.DemoHeaderId
        /// </remarks>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 8)]
        public string DemoFileStamp;
        /// <summary>
        /// Demo protocol
        /// </summary>
        /// <remarks>
        /// Demo protocol version, should be DemoConstants.DemoProtocol
        /// </remarks>
        [MarshalAs(UnmanagedType.I4)]
        public Int32 DemoProtocol;
        /// <summary>
        /// Network protocol
        /// </summary>
        /// <remarks>
        /// Network protocol version number, should be DemoConstants.ProtocolVersion
        /// </remarks>
        [MarshalAs(UnmanagedType.I4)]
        public Int32 NetworkProtocol;
        /// <summary>
        /// Server name
        /// </summary>
        /// <remarks>
        /// DemoConstants.MaxOsPathLength characters long
        /// </remarks>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DemoConstants.MaxOsPathLength)]
        public string ServerName;
        /// <summary>
        /// Client name
        /// </summary>
        /// <remarks>
        /// DemoConstants.MaxOsPathLength characters long
        /// </remarks>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DemoConstants.MaxOsPathLength)]
        public string ClientName;
        /// <summary>
        /// Map name
        /// </summary>
        /// <remarks>
        /// DemoConstants.MaxOsPathLength characters long
        /// </remarks>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DemoConstants.MaxOsPathLength)]
        public string MapName;
        /// <summary>
        /// Game directory
        /// </summary>
        /// <remarks>
        /// DemoConstants.MaxOsPathLength characters long
        /// </remarks>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DemoConstants.MaxOsPathLength)]
        public string GameDirectory;
        /// <summary>
        /// Playback time
        /// </summary>
        /// <remarks>
        /// The length of the demo, in seconds
        /// </remarks>
        [MarshalAs(UnmanagedType.R4)]
        public float PlaybackTime;
        /// <summary>
        /// Ticks
        /// </summary>
        /// <remarks>
        /// The number of ticks in the demo
        /// </remarks>
        [MarshalAs(UnmanagedType.I4)]
        public Int32 PlaybackTicks;
        /// <summary>
        /// Frames
        /// </summary>
        /// <remarks>
        /// The number of frames in the demo
        /// </remarks>
        [MarshalAs(UnmanagedType.I4)]
        public Int32 PlaybackFrames;
        /// <summary>
        /// Sign on length
        /// </summary>
        /// <remarks>
        /// Length of the signon data (init for first frame)
        /// </remarks>
        [MarshalAs(UnmanagedType.I4)]
        public Int32 SignonLength;

        /// <summary>
        /// Validates caller's properties
        /// </summary>
        /// <exception cref="HeaderValidationException">
        /// Raises an exception containing info about invalid properties.
        /// Checked values of properties can be found in Data property of exception
        /// where key is the name of the property and value is the actual value
        /// </exception>
        public void Validate()
        {
            try
            {
                if (DemoFileStamp != DemoConstants.DemoHeaderId)
                {
                    ThrowException("Invalid demo header", nameof(DemoFileStamp), DemoFileStamp);
                }

                if (DemoProtocol != DemoConstants.DemoProtocol)
                {
                    ThrowException("Invalid demo protocol", nameof(DemoProtocol), DemoProtocol);
                }

                if (NetworkProtocol != DemoConstants.ProtocolVersion)
                {
                    ThrowException("Invalid protocol version", nameof(NetworkProtocol), NetworkProtocol);
                }
            }
            catch (HeaderValidationException ex)
            {
                throw ex;
            }
        }

        private void ThrowException(string message, object key, object value)
        {
            var ex = new HeaderValidationException(message);
            ex.Data.Add(key, value);
            throw ex;
        }
    }
}
