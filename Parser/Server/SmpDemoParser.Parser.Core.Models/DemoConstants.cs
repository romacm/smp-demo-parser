﻿using System;

namespace SmpDemoParser.Parser.Core.Models
{
    public class DemoConstants
    {
        public const Int32 MaxOsPathLength = 260;
        public const Int32 DemoProtocol = 4;
        public const String DemoHeaderId = "HL2DEMO";
        public const Int32 FDemoNormal = 0;
        public const Int32 FDemoUseOrigin2 = 1 << 0;
        public const Int32 FDemoUseAngles = 1 << 1;
        public const Int32 FDemoNointerp = 1 << 2;
        public const Int32 MaxSplitScreenClients = 2;
        public const Int32 ProtocolVersion = 13634;
    }
}
