﻿namespace SmpDemoParser.Parser.Core.Models
{
    public enum DemoMessages
    {
        DemSignon = 1,
        DemPacket,
        DemSynctick,
        DemConsoleCmd,
        DemUserCmd,
        DemDataTables,
        DemStop,
        DemCustomData,
        DemStringTables,
        DemLastCmd = DemStringTables
    }
}
